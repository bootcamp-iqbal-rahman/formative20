package id.co.nexsoft.formative20.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.formative20.model.Result;

public interface ResultRepository extends JpaRepository<Result, Long> {
    
}