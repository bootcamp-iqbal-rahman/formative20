package id.co.nexsoft.formative20.controller;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.nexsoft.formative20.model.Result;
import id.co.nexsoft.formative20.utils.GetData;

@Controller
@RequestMapping("/")
public class ViewController {
    private static final String URL = "http://localhost:8080/api/result";
    
    @GetMapping
    public String index() {
        return "index";
    }

    @GetMapping("/result") 
    public String result(ModelMap model) {
        GetData getData = new GetData();
        HttpResponse<String> response = getData.getData(URL);
        List<Result> data = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        int temp = 0;
        String[][] returnData;

        try {
            JsonNode jsonNode = objectMapper.readTree(response.body());

            for (JsonNode personNode : jsonNode) {
                Result resultList = objectMapper.treeToValue(personNode, Result.class);
                data.add(resultList);
            }

            returnData = new String[data.size()][5];

            for (Result rsl : data) {
                returnData[temp][0] = Long.toString(rsl.getId());
                returnData[temp][1] = Integer.toString(rsl.getPostId());
                returnData[temp][2] = rsl.getName();
                returnData[temp][3] = rsl.getEmail();
                returnData[temp][4] = rsl.getBody();
                temp++;
            }

            model.put("data", returnData);
        } catch (Exception e) {
            System.out.println("error : " + e.getMessage());
            model.put("data", "");
        }

        return "result";
    }
}
