package id.co.nexsoft.formative20.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.formative20.model.Result;
import id.co.nexsoft.formative20.service.DefaultService;
import id.co.nexsoft.formative20.utils.StatusCode;

@RestController
@RequestMapping("/api/result")
public class ResultController extends StatusCode {
    @Autowired
    private DefaultService<Result> defaultService;

    @GetMapping
    public ResponseEntity<?> getAllData() {
        List<Result> data = defaultService.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createData(@RequestBody List<Result> data) {
        boolean result = defaultService.addData(data);
        if (result == false) {
            return new ResponseEntity<>(get422(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>(get201(), HttpStatus.CREATED);
    }
}
