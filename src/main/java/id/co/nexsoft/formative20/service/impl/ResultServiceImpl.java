package id.co.nexsoft.formative20.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.formative20.model.Result;
import id.co.nexsoft.formative20.repository.ResultRepository;
import id.co.nexsoft.formative20.service.DefaultService;

@Service
public class ResultServiceImpl implements DefaultService<Result> {
    @Autowired
    private ResultRepository repository;

    @Override
    public List<Result> getAllData() {
        return repository.findAll();
    }

    @Override
    public boolean addData(List<Result> data) {
        boolean isSuccess = true;

        for (Result rsl : data) {
            if (repository.save(rsl) == null) {
                isSuccess = false;
            }
        }
        return isSuccess;
    }
    
}
