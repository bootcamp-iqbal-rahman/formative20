package id.co.nexsoft.formative20.service;

import java.util.List;

public interface DefaultService<T> {
    List<T> getAllData();
    boolean addData(List<T> data);
}
