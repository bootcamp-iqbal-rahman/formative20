const URL_SOURCE = "https://jsonplaceholder.typicode.com/posts/1/comments"
const URL_POST = "http://localhost:8080/api/result"

const getData = (urlApi) => {
    return new Promise((resolve, reject) => {
        fetch(urlApi)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status} - ${response.statusText}`);
                }
                return response.json();
            })
            .then(data => resolve(data))
            .catch(error => reject(error));
    });
};

function postData(urlApi, data) {
    return fetch(urlApi, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then(response => response.json())
    .then(result => result.value)
    .catch(error => {
        console.error('Error posting data:', error);
        throw error; // Re-throw the error to propagate it down the chain
    });
}

function onClick() {
    getData(URL_SOURCE).then(data => {
        const resultPost = postData(URL_POST, data);
    })
}