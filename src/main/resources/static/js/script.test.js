const { getData } = require('/Users/NEXSOFT/Documents/bootcamp/formative/formative20/formative20/src/main/resources/static/js/script.js')

const url = 'https://anxious-clam-belt.cyclic.app/dompet/1';

test('fetch promise', async () => {
    const data = await getData(url);
    expect(data).toEqual([
        {
            id: 1,
            jenis: "Cash",
            deskripsi: "dompet, pegangan, fisik"
        }
    ]);
})