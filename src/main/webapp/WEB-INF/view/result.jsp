<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result - Formative 20 | Iqbal Rahman</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

    <div class="relative overflow-x-auto mt-6 ml-6 mr-6">
        <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        id
                    </th>
                    <th scope="col" class="px-6 py-3">
                        postId
                    </th>
                    <th scope="col" class="px-6 py-3">
                        name
                    </th>
                    <th scope="col" class="px-6 py-3">
                        email
                    </th>
                    <th scope="col" class="px-6 py-3">
                        body
                    </th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${data}">
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                            ${item[0]}
                        </th>
                        <td class="px-6 py-4">
                            ${item[1]}
                        </td>
                        <td class="px-6 py-4">
                            ${item[2]}
                        </td>
                        <td class="px-6 py-4">
                            ${item[3]}
                        </td>
                        <td class="px-6 py-4">
                            ${item[4]}
                        </td>
                    </tr>
                </c:forEach>
                
            </tbody>
        </table>
    </div>

    <script type="text/javascript" src="/js/script.js"></script>
</body>
</html>