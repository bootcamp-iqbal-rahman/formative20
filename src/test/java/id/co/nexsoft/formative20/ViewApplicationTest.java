package id.co.nexsoft.formative20;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import id.co.nexsoft.formative20.controller.ViewController;

@WebMvcTest(ViewController.class)
public class ViewApplicationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void indexView() throws Exception {
        mockMvc.perform(get("/"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("index"));
    }

    @Test
    void resultView() throws Exception {
        mockMvc.perform(get("/result"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("result"));
    }
}
