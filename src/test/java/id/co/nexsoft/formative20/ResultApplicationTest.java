package id.co.nexsoft.formative20;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.formative20.model.Result;

@SpringBootTest
@AutoConfigureMockMvc
public class ResultApplicationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllData() throws Exception {
        this.mockMvc.perform(get("/api/result"))
                        .andDo(print())
                        .andExpect(status().isOk());
    }

    @Test
    void postData() throws Exception {
        List<Result> resultList = new ArrayList<>();
        resultList.add(
            new Result(1, "name", "email", "body")
        );
        
        mockMvc.perform(post("/api/result")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(resultList))
            ).andExpect(status().isCreated());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }

    
}
